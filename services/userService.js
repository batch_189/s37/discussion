    const User = require('../models/User');
    const Course = require('../models/Course')
    const bcrypt = require('bcrypt');
    const auth = require('../auth');

// Check if email the already exists
    module.exports.isEmailExists = (requestBody) => {


	// The result is sent back to the frontend via the "then" method found in the route file
        return User.find({email: requestBody.email}).then(result  => {

            // The "find" method returns a record if a match is found
            return result.length > 0
        });
    };

// User Registration
    module.exports.registerUser = (requestBody) =>  {


	// Creates a variable "newUser" and instantiates a new "User" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information
        let newUser = new User ({
            firstName: requestBody.firstName,
            lastName: requestBody.lastName,
            email: requestBody.email,
            mobileNo: requestBody.mobileNo,
            password: bcrypt.hashSync(requestBody.password, 10) // HashSync (<dataToBeHash>, <saltRound>)
        });

        // Saves the created object to our database
        return newUser.save().then((user, error)  => {
            // if(error) {
            //     return false
            // } else {
            //     return user
            // }

            return error ? false : user;
        });
    };

// User Authentication
    module.exports.loginUser = (requestBody) => {

    // The "findOne" method returns the first record in the collection that matches the search criteria
	// We use the "findOne" method instead of the "find" method which returns all records that match the search criteria.
        return User.findOne({email: requestBody.email}).then(result => {

            // User does not exist
            if (result  == null) {
                return false

            // User exists   
            } else {

			// Creates the variable "isPasswordCorrect" to return the result of comparing the login form password and the database password
			// The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result.
                const isPasswordCorrect = bcrypt.compareSync(requestBody.password, result.password);
                // compareSync(<data>, <encryptPassword>) // will return true/false

                // If the passwords match/result of the above code is true
                if (isPasswordCorrect) {

                // Generate an access token
				// Uses the "createAccessToken" method defined in the "auth.js" file
				// Returning an object back to the frontend application is common practice to ensure information is properly labeled and real world examples normally return more complex information represented by objects.
                    return {access: auth.createAccessToken(result)}
                } else {
                    return false
                }
            }
        });
    };

// Retrieving specific user and reassigning properties value. (USING POST)
    module.exports.getUserProfile = (data) => {
        
        return User.findById(data.id).then(result => {

                console.log(result)

		    // Changes the value of the user's password to an empty string when returned to the frontend
		    // Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application.
                result.password = "";

            // Returns the user information with the password as an empty string
                return result;
        });
    };

// Retrieving specific user and reassigning properties value.(USING GET)
    // module.exports.getUserId = (requestBody) => {
    //     return User.findById(requestBody).then((result, error) => {
    //         if (error) {
    //             return error
    //         } else {
    //             result.password = "";
    //             return result;
    //         }
    //     });
    // };


// Enroll User to a class
    module.exports.enroll = async (data) => {
        console.log(data)

        let isUserUpdated = await User.findById(data.userId).then(user => {

            user.enrollments.push({courseId: data.courseId});
            return user.save().then((user, error) => {
                return error ? false : true
            });
        });
        console.log(isUserUpdated)

        let isCourseUpdated = await Course.findById(data.courseId).then(course => {
           
            course.enrollees.push({userId: data.userId});
            return course.save().then((course, error) => {
                return error ? false : true
            });
        });
        console.log(isCourseUpdated)

    return isUserUpdated && isCourseUpdated ? true : false
};

