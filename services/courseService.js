const Course = require('../models/Course');
const auth = require('../auth');
const User = require('../models/User');

// Create a new course
module.exports.addCourse = (requestBody, userData) => {

    return User.findById(userData.userId).then(result => {

        if (userData.isAdmin == false){
            return 'Only Admin Can Add Course'
        } else {
            let newCourse = new Course({
                name: requestBody.name,
                description: requestBody.description,
                price: requestBody.price
            });
            
            return newCourse.save().then((course, error) => {
                
                return error ? false : "Course Added Successfully!"
            });
        };
    });
};

// Retrieve all Courses
module.exports.getAllCourses = async (data) => {

    if(data.isAdmin) {
        return Course.find({}).then(result => {
            return result
        });
    } else {
        return "Course not found"
    };
};

// Retrieving all Active courses
module.exports.getActiveCourse = () => {

    return Course.find({isActive: true}).then(result => {
        return result
    });
};

// Retrieve Specific course by ID
module.exports.getCourse = (requestCourseId) => {

    return Course.findById(requestCourseId.courseId).then(result => {
        return result
    });
};

// Update A Course
module.exports.updateCourse = (requestUpdate, requestBody) => {
    
    let updatedCourse = {
        name: requestBody.name,
        description: requestBody.description,
        price: requestBody.price
    };
    return Course.findById(requestUpdate.courseId, updatedCourse).then((course, error) => {
        return error ? false : "Course Updated Successfully!"
    });
};

// Archive A Course
module.exports.courseArchive = (data ,requestBody) => {

    if (data.payload === true) {

        let updateActiveField = { isActive: requestBody.isActive };

        return Course.findById(data.courseId, updateActiveField).then((course, error) => {
            return error ? false : "Course Archived Successfully!"
        });
    } else {
        return false
    }
};
