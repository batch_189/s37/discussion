
const express = require('express');
const router = express.Router();
const courseService = require('../services/courseService');
const auth = require('../auth');


router.post('/', auth.verify, (request, response) => {

    const userData = auth.decode(request.headers.authorization)
    courseService.addCourse(request.body, {userId: userData.id, isAdmin:userData.isAdmin}).then(resultFromServices => response.send(resultFromServices));
});

router.get('/admin', auth.verify, (request, response) => {

    const userData = auth.decode(request.headers.authorization);
    courseService.getAllCourses(userData).then(resultFromServices => response.send(resultFromServices));
});

// Retrieving all Active Courses
router.get('/', (request, response) => {

    courseService.getActiveCourse().then(resultFromServices => response.send(resultFromServices));
});

// Route for retrieving a specific course
router.get('/:courseId', (request, response) => {
    // console.log(request.params);
    courseService.getCourse(request.params).then(resultFromServices => response.send(resultFromServices));
});

// Updating Course
router.put('/:courseId', auth.verify, (request, response) => {

    courseService.updateCourse(request.params, request.body).then(resultFromServices => response.send(resultFromServices));
});

router.put('/:courseId/archive', auth.verify, (request, response) => {

    const data = { 
        courseId: request.params.courseId,
        payload : auth.decode(req.headers.authorization).isAdmin
    }
    courseService.courseArchive(data, request.body).then(resultFromServices => response.send(resultFromServices));
});

module.exports = router;