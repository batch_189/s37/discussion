const express = require('express');
const router = express.Router();
const  userService = require('../services/userService')
const auth = require('../auth');

router.post("/checkEmail", (request,response) => {
    userService.isEmailExists(request.body).then(resultFromService => response.send (resultFromService))
});

router.post("/registration", (request, response) => {
    userService.registerUser(request.body).then(resultFromService => response.send (resultFromService))
});

router.post("/login", (request, response) => {
    userService.loginUser(request.body).then(resultFromService =>  response.send (resultFromService))
});

router.get("/details", auth.verify, (request, response) => {

    const userData = auth.decode(request.headers.authorization);
    console.log(userData);
    // {
    //     id: '62c2b864feef498fb3c56ffd',
    //     email: 'charles@gmail.com',
    //     isAdmin: false,
    //     iat: 1656986737
    // }
    console.log(request.headers.authorization);

    userService.getUserProfile({id: userData.id}).then(resultFromService => response.send (resultFromService))
});

// router.get("/:id", (request, response) => {
//     userService.getUserId(request.params.id).then(resultFromService => response.send (resultFromService))
// });

router.post('/enroll', auth.verify, (request, response) => {

    let data = {

        // User ID will be retrieve from the req. header
        isAdmin: auth.decode(request.headers.authorization).isAdmin,
        userId: auth.decode(request.headers.authorization).id,
        courseId: request.body.courseId
    }
    userService.enroll(data).then(resultFromService => response.send(resultFromService));
});

module.exports = router;