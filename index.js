/*
     MVP (minimum viable product) Requirements
        - that has enough features to be useful to its target
    viable - working/usable na in the first start/ first version then later on add other features while already on the market.
    
    JWT - only issued once user is verified, nagkakaron lang pag naglologin
      Header - type JWT and algorithm
      Payload - data about an entity/ user data
      Signature - token is verified, not tempered.
 */

  const express = require('express');
  const mongoose = require('mongoose');
// Allows our backend app to be available to out frontend app
  const cors = require('cors');
  const userRoutes = require('./controllers/userRoutes');
  const courseRoutes = require('./controllers/courseRoutes')

  const port = 4000;

  const app = express();

// Allows all resources to access our backend app
  app.use(cors());

  app.use(express.json());
  app.use(express.urlencoded({extended: true}));

// index.js is our main server
  app.use('/users', userRoutes);
  app.use('/courses', courseRoutes);

  mongoose.connect(`mongodb://127.0.0.1:27017/s37-s41`, {
  // mongoose.connect(`mongodb+srv://michella:admin123@cluster0.6wklp.mongodb.net/s37-s41?retryWrites=true&w=majority`, {

      useNewUrlParser: true,
      useUnifiedTopology: true
  });

  let db = mongoose.connection;

  db.on('error', () => console.error.bind(console, 'error'));
  db.once('open', () => console.log('Now connected to MongoDB atlas'));

  app.listen(process.env.PORT || port, () => {
    console.log(`API is now online on port ${process.env.PORT || port}`);
  });

