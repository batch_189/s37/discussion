const jwt  = require('jsonwebtoken');

const secretKey = "qwerty1234";

module.exports.createAccessToken = (user) => {
    
    // The data will be received from the registration form
    // When the user logs in, a token will be created with user's info.
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    };
    // Generate a JSON web token using the jwt's  sign method
    // Generate the token using the form data and the secret code with no additional opt. provided
    return jwt.sign(data, secretKey, {}) // (<payload>, secretKey, {})
};

module.exports.verify = (request, response, next) => {

// console.log(request.headers);
    let token = request.headers.authorization;

    if (typeof token !== "undefined") {
        // console.log(token);

        token = token.slice(7, token.length);
        return jwt.verify(token, secretKey, (error, data) => {

            if (error){
                return response.send({auth: "failed"})
            } else {
                next()
            }
        });
    } else {
        return response.send({auth: "failed"});
    }
};

module.exports.decode = (token) => {

    token = token.slice(7, token.length);
    return jwt.verify(token, secretKey, (error, data) => {
        if (error){
            return null
        } else {
            return jwt.decode(token, {complete: true}).payload
        }
    });

};